package com.ramesh.mindvalley_rameshkumar_android_test.retrofit;

import com.google.gson.JsonArray;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Ramesh Kumar on 8/7/16.
 */
public interface GetAPI {

    @GET("/{path}")
    public void getData(@Path(value = "path", encode = false) String path, Callback<JsonArray> response);
}
