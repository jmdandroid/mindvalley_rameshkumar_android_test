package com.ramesh.mindvalley_rameshkumar_android_test.retrofit;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ramesh.mindvalley_rameshkumar_android_test.R;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ramesh Kumar on 8/7/16.
 */
//https://github.com/square/retrofit/issues/773
public class HttpError {
    static Context activity;

    public static void showError(RetrofitError error, Context activity) {

        if (HttpError.activity == null && activity == null)
            return;
        else if (activity != null)
            HttpError.activity = activity;


        switch (error.getKind()) {
            case NETWORK:
                Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();

                break;
            case UNEXPECTED:
//                            throw error;
                Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show();

                break;
            case HTTP:
                Response response = error.getResponse();
                switch (response.getStatus()) {
                    case 400:           //BAD REQUEST
//                                    on400(error);

                        Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.error_400), Toast.LENGTH_SHORT).show();

                        break;
                    case 401:           //UNAUTHORIZED
//                                    on401(error);
                        Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.error_401), Toast.LENGTH_SHORT).show();

                        break;
                    case 403:           //FORBIDDEN
//                                    on403(error);
                        Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.error_403), Toast.LENGTH_SHORT).show();

                        break;
                    case 404:       //NOT FOUND
//                                    on404(error);
                        Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.error_404), Toast.LENGTH_SHORT).show();

                        break;
                    default:
                        Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.http_error), Toast.LENGTH_SHORT).show();

//                                    throw error;
                }
                break;
            case CONVERSION: {
                Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                Log.e("Conversion error", "Message" + error.getMessage());
                break;
            }
            default:
                Toast.makeText(HttpError.activity, activity.getResources().getString(R.string.http_error), Toast.LENGTH_SHORT).show();

//                            throw new IllegalStateException("Unknown error kind: " + error.getKind(), error);
        }

    }
}