package com.ramesh.mindvalley_rameshkumar_android_test.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by Ramesh Kumar on 8/7/16.
 */
public class RestAdapterSingleton {

    public static String URL = "http://pastebin.com/raw";
    static RestAdapter restAdapter;
    static RestAdapter.Builder builder;

    private RestAdapterSingleton() {
    }

    public static RestAdapter getAdapter() {

        if (restAdapter == null) {
            Gson gson = new GsonBuilder()
                    .create();
            RequestInterceptor requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Content-Type", "application/json");
                }
            };
            builder = new RestAdapter.Builder();
            restAdapter = builder.setEndpoint(URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL).setRequestInterceptor(requestInterceptor)
//                .setConverter(new GsonConverter(gson))
                    .build();


        }
        return restAdapter;
    }
}
