package com.ramesh.mindvalley_rameshkumar_android_test.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramesh Kumar on 8/5/2016.
 */
public class ModelUrls implements Serializable {

    private static final long serialVersionUID = 825755965111261894L;
    @SerializedName("raw")
    private String mRaw;
    @SerializedName("regular")
    private String mRegular;
    @SerializedName("full")
    private String mFull;
    @SerializedName("thumb")
    private String mThumb;
    @SerializedName("small")
    private String mSmall;

    public String getmRaw() {
        return mRaw;
    }

    public void setmRaw(String mRaw) {
        this.mRaw = mRaw;
    }

    public String getmRegular() {
        return mRegular;
    }

    public void setmRegular(String mRegular) {
        this.mRegular = mRegular;
    }

    public String getmFull() {
        return mFull;
    }

    public void setmFull(String mFull) {
        this.mFull = mFull;
    }

    public String getmThumb() {
        return mThumb;
    }

    public void setmThumb(String mThumb) {
        this.mThumb = mThumb;
    }

    public String getmSmall() {
        return mSmall;
    }

    public void setmSmall(String mSmall) {
        this.mSmall = mSmall;
    }
}
