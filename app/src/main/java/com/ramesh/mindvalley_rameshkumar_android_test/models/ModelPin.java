package com.ramesh.mindvalley_rameshkumar_android_test.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramesh Kumar on 8/5/2016.
 */
public class ModelPin implements Serializable{


    private static final long serialVersionUID = -5251257176178279695L;
    @SerializedName("id")
    private String mID;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("color")
    private String mColor;
    @SerializedName("likes")
    private String mLikes;
    @SerializedName("like_by_user")
    private boolean mLikeByUser;
    @SerializedName("urls")
    private ModelUrls mURLs;

    public ModelUser getmUser() {
        return mUser;
    }

    public void setmUser(ModelUser mUser) {
        this.mUser = mUser;
    }

    @SerializedName("user")
    private ModelUser mUser;
    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmColor() {
        return mColor;
    }

    public void setmColor(String mColor) {
        this.mColor = mColor;
    }

    public String getmLikes() {
        return mLikes;
    }

    public void setmLikes(String mLikes) {
        this.mLikes = mLikes;
    }

    public boolean ismLikeByUser() {
        return mLikeByUser;
    }

    public void setmLikeByUser(boolean mLikeByUser) {
        this.mLikeByUser = mLikeByUser;
    }

    public ModelUrls getmURLs() {
        return mURLs;
    }

    public void setmURLs(ModelUrls mURLs) {
        this.mURLs = mURLs;
    }

}
