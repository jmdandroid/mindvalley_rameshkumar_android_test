package com.ramesh.mindvalley_rameshkumar_android_test.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramesh Kumar on 8/5/2016.
 */
public class ModelProfileImage implements Serializable {

    private static final long serialVersionUID = -757025717213354621L;
    @SerializedName("small")
    private String mSmall;

    @SerializedName("medium")
    private String mMedium;

    public String getmLarge() {
        return mLarge;
    }

    public void setmLarge(String mLarge) {
        this.mLarge = mLarge;
    }

    public String getmSmall() {
        return mSmall;
    }

    public void setmSmall(String mSmall) {
        this.mSmall = mSmall;
    }

    public String getmMedium() {
        return mMedium;
    }

    public void setmMedium(String mMedium) {
        this.mMedium = mMedium;
    }

    @SerializedName("large")
    private String mLarge;

}
