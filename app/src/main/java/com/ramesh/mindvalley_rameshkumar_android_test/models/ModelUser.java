package com.ramesh.mindvalley_rameshkumar_android_test.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramesh Kumar on 8/5/2016.
 */
public class ModelUser implements Serializable {

    private static final long serialVersionUID = -7115822068031971337L;
    @SerializedName("id")
    private String mID;
    @SerializedName("username")
    private String mUserName;
    @SerializedName("name")
    private String mName;
    @SerializedName("profile_image")
    private ModelProfileImage modelProfileImage;

    public ModelProfileImage getModelProfileImage() {
        return modelProfileImage;
    }

    public void setModelProfileImage(ModelProfileImage modelProfileImage) {
        this.modelProfileImage = modelProfileImage;
    }

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

}
