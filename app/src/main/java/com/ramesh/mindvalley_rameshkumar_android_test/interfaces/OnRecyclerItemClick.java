package com.ramesh.mindvalley_rameshkumar_android_test.interfaces;

import android.view.View;

/**
 * Created by Ramesh Kumar on 8/8/2016.
 */
public interface OnRecyclerItemClick {
    void onItemClick(Object item, View view, int position);
}
