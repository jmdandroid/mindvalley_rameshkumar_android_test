package com.ramesh.mindvalley_rameshkumar_android_test.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ramesh.mindvalley_rameshkumar_android_test.R;
import com.ramesh.mindvalley_rameshkumar_android_test.interfaces.OnRecyclerItemClick;
import com.ramesh.mindvalley_rameshkumar_android_test.models.ModelPin;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by Ramesh Kumar on 8/5/2016.
 */


public class AdapterImagesRecyclerView extends RecyclerView.Adapter<AdapterImagesRecyclerView.ViewHolder> {
    public static RecyclerView rView;
    int resourceID;
    Context context;
    ViewHolder vh;
    OnRecyclerItemClick onRecyclerItemClick;
    private ArrayList<ModelPin> mDataset;

    public AdapterImagesRecyclerView(Context context, ArrayList<ModelPin> list, RecyclerView mRecyclerView, int resID, OnRecyclerItemClick onRecyclerItemClick) {
        mDataset = list;
        rView = mRecyclerView;
        resourceID = resID;
        this.context = context;

        this.onRecyclerItemClick = onRecyclerItemClick;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterImagesRecyclerView(ArrayList<ModelPin> myDataset) {
        mDataset = myDataset;
    }


    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if (mDataset != null)
            return mDataset.get(mDataset.size() - 1) != null ? 0 : 1;
        return 0;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AdapterImagesRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        View v;
        v = LayoutInflater.from(context)
                .inflate(resourceID, parent, false);
        // set the view's size, margins, paddings and layout parameters
        vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        populateItem(holder, ((ModelPin) mDataset.get(position)), position);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mDataset == null)
            return 0;
        return mDataset.size();
    }

    void downloadImage(final String image, final String fileName, final ImageView iv, final ProgressBar progressBar) {
        Picasso.with(context).load(image)
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                        iv.setImageBitmap(bitmap);
                        AsyncTask asyncTask = new AsyncTask() {
                            @Override
                            protected Object doInBackground(Object[] params) {
                                try {
                                    String root = Environment.getExternalStorageDirectory().toString();
                                    File myDir = new File(root + "/Android/data/" + context.getPackageName() + "/images/");
                                    if (!myDir.exists()) {
                                        myDir.mkdirs();
                                    }
                                    myDir = new File(myDir, fileName);
                                    FileOutputStream out = new FileOutputStream(myDir);
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                                    out.flush();
                                    out.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        };
                        asyncTask.execute();
//                    bitmap.recycle();
                        Log.d("IMAGE DOWNLOADED ", image);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Image Downloaded", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.mItem == null)
            return;
        if (holder.mItem.getmURLs().getmRegular() != null)
            Picasso.with(context).invalidate(holder.mItem.getmURLs().getmRegular());
        Log.e("VIEW RECycled", holder.mItem.getmURLs().getmRegular() + "");
    }

    public void addNull() {
        mDataset.add(null);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void removeNull() {
        if (mDataset.get(mDataset.size() - 1) == null) {
            mDataset.remove(mDataset.size() - 1);
            notifyItemRemoved(mDataset.size());
        }
    }

    public void populateItem(final ViewHolder viewHolder, final ModelPin item, final int position) {
        if (item == null)
            return;
        viewHolder.setPosition(position);
        try {
            final Callback callback = new Callback() {
                @Override
                public void onSuccess() {
//                viewHolder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
//                viewHolder.progressBar.setVisibility(View.GONE);
                }
            };

            viewHolder.iv.setBackgroundColor(Color.parseColor(item.getmColor()));
            if (item.getmURLs().getmRegular() != null) {

                Picasso.with(context).load(item.getmURLs().getmRegular()).into(viewHolder.iv, callback);

            } else {


                Picasso.with(context).load(R.drawable.placeholder).skipMemoryCache().placeholder(R.drawable.placeholder).into(viewHolder.iv, callback);

                Log.d("IMAGE URL Null", position + "");
            }

            Target target
                    = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), bitmap);
                    drawable.setCircular(true);
                    viewHolder.ivProfile.setImageDrawable(drawable);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            if (item.getmUser().getModelProfileImage().getmSmall() != null) {
                Picasso.with(context).load(item.getmUser().getModelProfileImage().getmSmall()).skipMemoryCache().placeholder(R.drawable.placeholder).into(target);
            } else
                Picasso.with(context).load(R.drawable.placeholder).skipMemoryCache().placeholder(R.drawable.placeholder).into(target);

            if (item.getmUser() != null)
                viewHolder.tvName.setText(" " + item.getmUser().getmName());
            else viewHolder.tvName.setText("Unknown");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView tvName;
        ImageView iv, ivProfile;
        View v;
        ProgressBar progressBar;
        private ModelPin mItem;
        private int position;


        public ViewHolder(View v) {
            super(v);
            this.v = v;
            v.setOnClickListener(this);
            tvName = (TextView) v.findViewById(R.id.tv_name);
            ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
            iv = (ImageView) v.findViewById(R.id.iv);

        }

        @Override
        public void onClick(View view) {
            onRecyclerItemClick.onItemClick(mDataset.get(position), view, position);
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

}
