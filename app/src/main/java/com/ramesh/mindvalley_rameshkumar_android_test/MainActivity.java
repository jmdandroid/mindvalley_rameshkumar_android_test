package com.ramesh.mindvalley_rameshkumar_android_test;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.ramesh.mindvalley_rameshkumar_android_test.adapters.AdapterImagesRecyclerView;
import com.ramesh.mindvalley_rameshkumar_android_test.interfaces.OnRecyclerItemClick;
import com.ramesh.mindvalley_rameshkumar_android_test.models.ModelPin;
import com.ramesh.mindvalley_rameshkumar_android_test.retrofit.GetAPI;
import com.ramesh.mindvalley_rameshkumar_android_test.retrofit.HttpError;
import com.ramesh.mindvalley_rameshkumar_android_test.retrofit.RestAdapterSingleton;
import com.ramesh.mindvalley_rameshkumar_android_test.utils.CommonActions;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements OnRecyclerItemClick {

    RecyclerView mRecyclerView;
    AdapterImagesRecyclerView adapterMenu;

    ProgressBar loadMoreView;
    CommonActions commonActions;
    SwipeRefreshLayout swipeRefreshLayout;
    boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        commonActions = new CommonActions(this);
        initViews();
        getDataTask();


    }

    private void getDataTask() {
        isRefresh = true;
        String response = commonActions.getValueString(CommonActions.KEY_RESPONSE, null);
        if (response != null) {
            parseValues(response);
        } else getDataFromServer();
    }

    private void getDataFromServer() {

        if (!commonActions.hasConnection()) {
//            Toast.makeText(this, "No internet access, kindly try later", Toast.LENGTH_SHORT).show();
            Snackbar.make(findViewById(R.id.coordinate_layout), "No internet access, kindly try later", Snackbar.LENGTH_LONG);
        }
        RestAdapterSingleton.getAdapter().create(GetAPI.class).getData("wgkJgazE", new Callback<JsonArray>() {
            @Override
            public void success(JsonArray jsonElements, Response response) {


                parseValues(jsonElements.toString());
                commonActions.saveUserPreferences(CommonActions.KEY_RESPONSE, jsonElements.toString());
                isRefresh = false;
            }

            @Override
            public void failure(RetrofitError error) {
                HttpError.showError(error, MainActivity.this);
                isRefresh = false;
//                Snackbar.make(findViewById(R.id.coordinate_layout), "No internet access, kindly try later", Snackbar.LENGTH_LONG);
            }
        });

    }

    private void parseValues(String jsonArray) {
        Log.e("Response", jsonArray.toString());
        Type listType = new TypeToken<ArrayList<ModelPin>>() {
        }.getType();
        ArrayList<ModelPin> list = new Gson().fromJson(jsonArray, listType);
        populateValues(list);
    }

    private void initViews() {


        loadMoreView = (ProgressBar) findViewById(R.id.load_more);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefresh) {
                    isRefresh = true;
                    getDataFromServer();
                } else
                    swipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    private void populateValues(ArrayList<ModelPin> list) {

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
//        mRecyclerView.setItemViewCacheSize(0);
//            mRecyclerView.addItemDecoration(new DividerRecyclerView(10));
        adapterMenu = new AdapterImagesRecyclerView(this, list, mRecyclerView, R.layout.row_pin, this);
        mRecyclerView.setAdapter(adapterMenu);
        mRecyclerView.setVisibility(View.VISIBLE);
        loadMoreView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);


    }

    @Override
    public void onItemClick(Object item, View view, int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("model", (ModelPin) item);
            intent.putExtras(b);
            startActivity(intent,
//                            ActivityOptions.makeThumbnailScaleUpAnimation(iv1,
//                            bitmap,100, 200).toBundle()
//                            ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle()
//                            ActivityOptions.makeClipRevealAnimation(v,0,0,10,10).toBundle()
                    ActivityOptions.makeScaleUpAnimation(view, 0, 0, 10, 10).toBundle()
            );
        } else {

        }
    }
}
