package com.ramesh.mindvalley_rameshkumar_android_test.utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Sheharyar on 8/5/2016.
 */
public class ImageDownload {

    // ///////////////////////////////////////////////////////////////////////////////
    // download and store on disk
    public void DownloadFromUrl(String path) { // this is
        // the
        // downloader
        // method
        String[] spitPath = path.split("/");
        String fileName = spitPath[spitPath.length - 1];
        try {
            File file;
            URL url = new URL(path);
            Log.d("", "testing url" + url);
//            if (helper.isExternalStorageWritable()) {
//                Log.d("", "testing Environment.getExternalStorageDirectory()"
//                        + Environment.getExternalStorageDirectory() + " str "
//                        + Environment.getExternalStorageDirectory().toString());
//                // File dir = new File(Environment.getExternalStorageDirectory()
//                // + "/ieventapp/");
//                if (!dir.exists()) {
//                    dir.mkdirs();
//                }
//
//                file = new File(dir, fileName);
//
//            } else {
//
//
//                file = new File(getApplicationContext().getFilesDir(), fileName);
//            }

            long startTime = System.currentTimeMillis();
            Log.d("ImageManager", "download begining");
            Log.d("ImageManager", "download url:" + url);
            Log.d("ImageManager", "downloaded file name:" + fileName);
            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

			/*
             * Define InputStreams to read from the URLConnection.
			 */
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

			/*
             * Read bytes to the Buffer until there is nothing more to read(-1).
			 */
//            ByteArrayBuffer baf = new ByteArrayBuffer(50);
//            int current = 0;
//            while ((current = bis.read()) != -1) {
//                baf.append((byte) current);
//            }
//
//			/* Convert the Bytes read to a String. */
//            FileOutputStream fos = new FileOutputStream(file);
//            fos.write(baf.toByteArray());
//            fos.close();
            Log.d("ImageManager",
                    "download ready in"
                            + ((System.currentTimeMillis() - startTime) / 1000)
                            + " sec");

        } catch (IOException e) {
            Log.d("ImageManager", "Error: " + e);
        }

    }
}
