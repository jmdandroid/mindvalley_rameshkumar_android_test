package com.ramesh.mindvalley_rameshkumar_android_test.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CommonActions {

    public static final String PREF_FILE = "preferenceRamesh";
    public static final String KEY_RESPONSE = "response";
    Activity currentActivity;

    public CommonActions(Activity activity) {
        // TODO Auto-generated constructor stub
        this.currentActivity = activity;

    }


    public CommonActions(Context activity) {
        // TODO Auto-generated constructor stub
        this.currentActivity = (Activity) activity;

    }

    public void savePreferences(String key, boolean value) {
        SharedPreferences sharedPreferences = currentActivity
                .getSharedPreferences(PREF_FILE,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void saveUserPreferences(String key, String value) {
        SharedPreferences sharedPreferences = currentActivity
                .getSharedPreferences(PREF_FILE,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getValueString(String key, String default_value) {

        SharedPreferences sharedPreferences = currentActivity
                .getSharedPreferences(PREF_FILE,
                        Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, default_value);
    }

    public boolean hasConnection() {

        try {
            ConnectivityManager cm = (ConnectivityManager) currentActivity
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo wifiNetwork = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiNetwork != null && wifiNetwork.isConnected()) {
                return true;
            }

            NetworkInfo mobileNetwork = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobileNetwork != null && mobileNetwork.isConnected()) {
                return true;
            }

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return false;
    }


    public void clearAllPreferences() {
        SharedPreferences sharedPreferences = currentActivity
                .getSharedPreferences(PREF_FILE,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }


}
