package com.ramesh.mindvalley_rameshkumar_android_test;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ramesh.mindvalley_rameshkumar_android_test.models.ModelPin;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Ramesh Kumar on 8/8/2016.
 */
public class DetailActivity extends AppCompatActivity {
    ImageView iv, ivProfile;
    ModelPin modelPin;
    TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        modelPin = (ModelPin) getIntent().getExtras().getSerializable("model");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        initViews();
        populateValues();

    }

    private void initViews() {
        tvName = (TextView) findViewById(R.id.tv_name);
        ivProfile = (ImageView) findViewById(R.id.iv_profile);
        iv = (ImageView) findViewById(R.id.iv);
    }

    private void populateValues() {
        try {
            final Callback callback = new Callback() {
                @Override
                public void onSuccess() {
//               progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
//               progressBar.setVisibility(View.GONE);
                }
            };

            iv.setBackgroundColor(Color.parseColor(modelPin.getmColor()));
            if (modelPin.getmURLs().getmRegular() != null) {

                Picasso.with(DetailActivity.this).load(modelPin.getmURLs().getmRegular()).into(iv, callback);

            } else {


                Picasso.with(DetailActivity.this).load(R.drawable.placeholder).skipMemoryCache().placeholder(R.drawable.placeholder).into(iv, callback);

            }

            Target target
                    = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(DetailActivity.this.getResources(), bitmap);
                    drawable.setCircular(true);
                    ivProfile.setImageDrawable(drawable);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            if (modelPin.getmUser().getModelProfileImage().getmSmall() != null) {
                Picasso.with(DetailActivity.this).load(modelPin.getmUser().getModelProfileImage().getmSmall()).skipMemoryCache().placeholder(R.drawable.placeholder).into(target);
            } else
                Picasso.with(DetailActivity.this).load(R.drawable.placeholder).skipMemoryCache().placeholder(R.drawable.placeholder).into(target);

            if (modelPin.getmUser() != null)
                tvName.setText(" " + modelPin.getmUser().getmName());
            else tvName.setText("Unknown");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
